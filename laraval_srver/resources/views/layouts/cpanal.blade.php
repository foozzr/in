<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>




  <!--########################-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{ URL::asset('bootstrap/css/mdb.min.css') }}" rel="stylesheet">
  <!--########################-->




</head>
<body>
    <div id="app">
@guest
  @if (Route::has('register'))
    @include('layouts.PreNav')
  @endif
  @else
    @include('layouts.AfterNav')
@endguest
        <main class="py-4">
            @yield('content')
        </main>
    </div>


     <script type="text/javascript" src="{{ URL::asset('bootstrap/js/jquery-3.4.1.min.js')}}"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ URL::asset('bootstrap/js/popper.min.js')}}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ URL::asset('bootstrap/js/mdb.min.js')}}"></script>







</body>
</html>
